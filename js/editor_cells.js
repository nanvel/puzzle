var EditorCell = new Class(Element, {

    initialize: function(pos_x, pos_y) {

        var params = {'class': 'cell'};
        this.$super('div', params);

        this.addClass('filled');

        this.val = '';
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        this.q_x = '';
        this.q_y = '';
        this.focused = false;

        this.on('mousedown', function(event) {
            if (event.which==1) {
                this.fire('unfocus')
                this.focus();
                event.stop();
            };
        });

        this.on('contextmenu', 'stopEvent');
    },

    focus: function() {
        this.addClass('focused');
        this.focused = true;
    },

    unfocus: function() {
        this.removeClass('focused');
        this.focused = false;
    },

    set_char: function(c) {
        if(this.focused) {
            if(c==' '){
                this.addClass('filled');
                this.html('');
                this.val = '';
            } else {
                this.removeClass('filled');
                this.html(c);
                this.val = c;
            }
        }
    },

    set_questions: function(q) {
        this.q_x = q[0];
        this.q_y = q[1];
    }
});

var ManageCell = new Class(Element, {

    initialize: function(is_row) {

        var params = {'class': 'cell'};
        this.$super('div', params);

        this.addClass('empty');

        this.status = 0; //0-empty, 1-plus, 2-min

        this.on('mousedown', function(event) {
            if (this.status!=0 && event.which==1) {
                if(is_row) {
                    if(this.status==1) {
                        this.fire('plus_row');
                    } else {
                        this.fire('minus_row');
                    }
                } else {
                    if(this.status==1) {
                        this.fire('plus_column');
                    } else {
                        this.fire('minus_column');
                    }
                }
                event.stop();
            };
        });

        this.on('contextmenu', 'stopEvent');
    },

    set_status: function(s) {
        this.status = s;
        if(s=='0') {
            this.removeClass('cell_plus');
            this.removeClass('cell_minus');
            this.addClass('empty');
            this.html('');
        };
        if(s=='1') {
            this.removeClass('empty');
            this.removeClass('cell_minus');
            this.addClass('cell_plus');
            this.html('+');
        };
        if(s=='2') {
            this.removeClass('empty');
            this.removeClass('cell_plus');
            this.addClass('cell_minus');
            this.html('-');
        };
        
    }
});
