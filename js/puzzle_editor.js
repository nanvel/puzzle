var PuzzleEditor = new Class(Element, {

    initialize: function(data) {
        this.$super('div', {'class': 'field'});

        this.data = data;

        this.w = -1;
        this.h = -1;
        
        for(var i=0; i<data['cells'].length; i++) {
            if(data['cells'][i][0]>this.w){
                this.w = data['cells'][i][0];
            };
            if(data['cells'][i][1]>this.h){
                this.h = data['cells'][i][1];
            }
        };

        this.w += 1;
        this.h += 1;

        this.build();
        
        this.on('unfocus', this.unfocus);
        this.on('plus_row', this.plus_row);
        this.on('plus_column', this.plus_column);
        this.on('minus_row', this.minus_row);
        this.on('minus_column', this.minus_column);
    },

    build: function() {
        this.cells = [];
        this.rows = [];
        this.row_manage_cells = [];
        this.col_manage_cells = [];
        var row, m=[];
        // upper empty line
        row = new Element('div').addClass('row').insertTo(this);
        this.rows.push(row);
        for(var i=0; i<this.w+2; i++) {
            this.col_manage_cells.push(new ManageCell(false).insertTo(row));
        }
        this.col_manage_cells[this.col_manage_cells.length-2].set_status(2);
        this.col_manage_cells[this.col_manage_cells.length-1].set_status(1);
        if(this.w==0 || this.h==0){
            //empty field
            row = new Element('div').addClass('row').insertTo(this);
            this.rows.push(row);
            this.row_manage_cells.push(new ManageCell(true).insertTo(row));
            this.row_manage_cells[this.row_manage_cells.length-1].set_status(2);
            this.cells.push([new EditorCell(0, 0).insertTo(row)]);
            row = new Element('div').addClass('row').insertTo(this);
            this.rows.push(row);
            this.row_manage_cells.push(new ManageCell(true).insertTo(row));
            this.row_manage_cells[this.row_manage_cells.length-1].set_status(1);
            
        } else {
            for(var j=0; j<this.h; j++){
                row = new Element('div').addClass('row').insertTo(this);
                this.rows.push(row);
                this.row_manage_cells.push(new ManageCell(true).insertTo(row));
                if(j==this.h-1) {
                    this.row_manage_cells[this.row_manage_cells.length-1].set_status(2);
                }
                m = [];
                for(var i=0, c; i<this.w; i++){
                    c = new EditorCell(i, j);
                    c.insertTo(row);
                    m.push(c);
                }
                this.cells.push(m);
            };
            row = new Element('div').addClass('row').insertTo(this);
            this.rows.push(row);
            this.row_manage_cells.push(new ManageCell(true).insertTo(row));
            this.row_manage_cells[this.row_manage_cells.length-1].set_status(1);
            console.log('cells_len =' + this.data['cells'].length)
            for(var i=0; i<this.data['cells'].length; i++){
                this.cells[this.data['cells'][i][1]][this.data['cells'][i][0]].set_questions(this.get_questions(i));
                this.cells[this.data['cells'][i][1]][this.data['cells'][i][0]].set_char(this.data['answers'][i]);
            }
        }
    },

    set_char: function(c) {
        this.cells.each('each', 'set_char', c);
    },

    unfocus: function(event) {
        this.cells.each('each', 'unfocus');
    },

    get_questions: function(i) {
        var qss = [];
        if(this.data['cells'][i][2]){
            qss.push(this.data['questions'][this.data['cells'][i][2]-1]);
        } else {
            qss.push('');
        };
        if(this.data['cells'][i][3]){
            qss.push(this.data['questions'][this.data['cells'][i][3]-1]);
        } else {
            qss.push('');
        };
        return qss;
    },

    plus_row: function() {
        this.row_manage_cells[this.h-1].set_status(0);
        this.row_manage_cells[this.h].set_status(2);
        this.cells.push([]);
        for(var i=0, c; i<this.w; i++){
            c = new EditorCell(i, this.w);
            c.insertTo(this.rows[this.h+1]);
            this.cells[this.h].push(c);
        }
        var row = new Element('div').addClass('row').insertTo(this);
        this.rows.push(row);
        this.row_manage_cells.push(new ManageCell(true).insertTo(row));
        this.row_manage_cells[this.h+1].set_status(1);
        this.h +=1 ;
    },

    minus_row: function() {
        if(this.rows.length>3) {
            this.row_manage_cells[this.h-2].set_status(2);
            this.row_manage_cells[this.h-1].set_status(1);
            this.rows[this.h+1].remove();
            this.rows.splice(this.h+1);
            this.cells[this.h-1].each('remove');
            this.cells.splice(this.h-1);
            this.row_manage_cells.splice(this.h);
            this.h -= 1;
        }
    },

    plus_column: function() {
        this.col_manage_cells[this.w].set_status(0);
        this.col_manage_cells[this.w+1].set_status(2);
        this.col_manage_cells.push(new ManageCell(false).insertTo(this.rows[0]));
        this.col_manage_cells[this.w+2].set_status(1);
        for(var i=0; i<this.cells.length; i++) {
            this.cells[i].push(new EditorCell(this.h, i).insertTo(this.rows[i+1]));
        }
        this.w += 1;
    },

    minus_column: function() {
        if(this.w>1) {
            this.col_manage_cells[this.w].set_status(1);
            this.col_manage_cells[this.w-1].set_status(2);
            this.col_manage_cells[this.w+1].remove();
            this.col_manage_cells.splice(this.w+1);
            for(var i=0; i<this.h; i++) {
                this.cells[i][this.w-1].remove();
                this.cells[i].splice(this.w-1);
            }
            this.w -= 1;
        }
    },
});
