var Puzzle = new Class(Element, {

    direction: 0, //0-stop, 1-right, 2-down
    focus: -1,

    initialize: function(data) {
        this.$super('div', {'class': 'field'});

        this.data = data;

        // tooltips init
        $ext(Tooltip.Options, {
            delay: 200,
            fxName: null
        });

        this.build();
        
        this.on('focused', this.change_focus);
        this.on('move_focus', this.move_focus);
    },

    build: function() {
        this.cells = [];
        var x=0, y=0, row;
        
        row = new Element('div').addClass('row').insertTo(this);
        for(var i=0; i<(this.data['cells']).length; i++) {
            if(this.data['cells'][i][1]>y) {
                //next line
                y++;
                x = 0;
                row = new Element('div').addClass('row').insertTo(this);
            }
            for(var j=x; j<this.data['cells'][i][0]; j++) {
                this.cells.push(new Cell(j, this.data['cells'][i][1], '', '', this.cells.length).insertTo(row));
            }
            this.cells.push(new Cell(this.data['cells'][i][0], this.data['cells'][i][1], this.get_questions(i), this.cells.length).insertTo(row));
            x = this.data['cells'][i][0]+1;
        };
    },

    set_char: function(c) {
        if(this.focus!=-1) {
            this.cells[this.focus].set_char(c);
        }
    },

    change_focus: function(event) {
        this.cells.each('unfocus');
        this.focus = event.target.pos;
        event.target.focus();
    },
    
    find_cell: function(x, y) {
        for(i=0; i<this.cells.length; i++) {
            if(!this.cells[i].is_empty && this.cells[i].pos_x==x && this.cells[i].pos_y==y) {
                return this.cells[i];
            }
        }
        return false;
    },
    
    get_direction: function(dr, dd) {
        if(!dr && !dd) {
            return 0;
        };
        if(dr && dd && this.direction) {
            return this.direction;
        };
        if(dr) {
            return 1;
        } else {
            return 2;
        };
    },
    
    move_focus: function(event) {
        var dr = this.find_cell(event.target.pos_x+1, event.target.pos_y);
        var dd = this.find_cell(event.target.pos_x, event.target.pos_y+1);
        this.direction = this.get_direction(dr, dd);
        if(this.direction) {
            if(this.direction==1) {
                dr.fire('focused');
            } else {
                dd.fire('focused');
            }
        }
    },

    get_questions: function(i) {
        var qss = '';
        if(this.data['cells'][i][2]){
            qss = '&rarr; ' + this.data['questions'][this.data['cells'][i][2]-1] + '<br>'
        };
        if(this.data['cells'][i][3]){
            qss += '&darr; ' + this.data['questions'][this.data['cells'][i][3]-1]
        };
        return qss;
    }
});
