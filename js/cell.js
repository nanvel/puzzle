var Cell = new Class(Element, {

    initialize: function(pos_x, pos_y, qss, pos) {

        var params = {'class': 'cell'};

        if(qss) {
            params['data-tooltip'] = '';
            params['title'] = qss;
        };

        this.$super('div', params);

        this.is_empty = false;
        if(!qss) {
            this.is_empty = true;
            this.addClass('empty');
        };

        this.pos = pos;
        this.pos_x = pos_x;
        this.pos_y = pos_y;

        this.on('mousedown', function(event) {
            if (event.which==1 && !this.is_empty) {
                this.fire('focused');
                event.stop();
            };
        });

        this.on('contextmenu', 'stopEvent');
    },

    focus: function() {
        if(!this.is_empty) {
            this.addClass('focused');
        }
    },

    unfocus: function() {
        this.removeClass('focused');
    },
    
    set_char: function(c) {
        this.html(c);
        this.fire('move_focus');
    },
});
